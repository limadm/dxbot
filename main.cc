#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"lua51.lib")
#pragma comment(lib,"libtesseract302.lib")
#pragma comment(lib,"liblept168.lib")

#include <time.h>
#include <stdlib.h>
#include <windows.h>

extern "C" {
#include <luajit.h>
#include <lualib.h>
#include <lauxlib.h>
#include "lua-api.h"
}

#include "api.h"

using namespace dxbot;

Config dxbot::config = {
	1,                  // debug flag
	200, 5, 200, 25,    // input
	3, 206, 0.6, 0.5,   // text ocr
	64, 0, 17, 64, 48,  // icon ocr
	0, 0, 64, 20        // icon text ocr
};
static lua_State * L = 0;

void dxbot::Init() {
	InitDB();
}

void dxbot::SetConfig(Config cfg) {
	config = cfg;
}

void dxbot::StandBy(float minutes) {
	DxRelease();
	lua_gc(L, LUA_GCCOLLECT, 0);
	EndOCR();
	Sleep((long)(60000 * minutes));
}

#define CHECK(x,error) if (err = (x)) {					\
	wsprintf(msg, error L".\nError code: %d", err);		\
	MessageBox(NULL, msg, L"Oops...", MB_ICONERROR);	\
	goto clean;											\
}

int main() {
	int err = 0;
	wchar_t msg[100];
	srand((unsigned) time(NULL));
	L = luaL_newstate();
	CHECK(!L, "Cannot start program");
	luaL_openlibs(L);
	CHECK(luaL_loadbuffer(L, luaJIT_BC_api, luaJIT_BC_api_SIZE, "<bytecode>"),
		L"Cannot load runtime");
	lua_call(L, 0, 0);
	CHECK(luaL_loadfile(L, "script.lua"), L"Cannot load [script.lua]");
	lua_call(L, 0, 0);
clean:
	if (L) lua_close(L);
	return err;
}