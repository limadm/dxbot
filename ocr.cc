#include <cstdio>
#include <leptonica/allheaders.h>
#include <tesseract/baseapi.h>

#include "api.h"
#include "float3.h"
#include "icon.h"

using namespace tesseract;
using namespace dxbot;

namespace dxbot
{
	struct ColorDistribution {
		float3 mean, stdev;

		ColorDistribution(PIX* p, int x, int y, int w, int h) :
			mean(),
			stdev()
		{
			for (int _y = y; _y < y+h; _y++)
				for (int _x = x; _x < x+w; _x++) {
					l_uint32 val;
					pixGetPixel(p, _x, _y, &val);
					mean += float3::pixel(val);
				}
			mean /= (w*h);
			for (int _y = y; _y < y + h; _y++)
				for (int _x = x; _x < x + w; _x++) {
					l_uint32 val;
					pixGetPixel(p, _x, _y, &val);
					float3 d = mean - float3::pixel(val);
					stdev += d * d;
				}
			stdev /= (w*h);
			stdev = stdev.sqrt();
		}

		float Distance(const ColorDistribution &that) const {
			float3 d = mean - that.mean;
			return (d * d).sum();
		}
	};

	static TessBaseAPI *ocr = nullptr;
	void pixGammaNorm(PIX*, float gamma);
	l_uint32 pixelAdd(l_uint32 a, l_uint32 b);
	l_uint32 pixelSub(l_uint32 a, l_uint32 b);
	PIX* AlignToIcon(PIX *img, PIX *icon);
	PIX* ColorCorrection(PIX *img, PIX *icon);
	char * Trim(char*);
	const char * ScanText(PIX*);
	const char * ScanIcon(PIX*);
}

void dxbot::pixGammaNorm(PIX *pix, float gamma)
{
	// compute first histogram peak (32 bins)
	int minval = 0;
	constexpr int N = 64;
	static l_uint8 freq[N];
	for (int y = 0; y < pix->h; y++)
		for (int x = 0; x < pix->w; x++)
			freq[GET_DATA_BYTE(pix->data, (pix->w * y + x)) * N / 256]++;
	for (int i = 0; i < N-2; i++)
		if ((i == 0 || freq[i - 1] < freq[i]) && (freq[i] > freq[i + 1])) {
			minval = (i * 256 / N);
			break;
		}
	// compute minimum value in top border (y = 0)
	int maxval = 255;
	for (int x = 0; x < pix->w; x++) {
		int v0 = GET_DATA_BYTE(pix->data, x);
		if (v0 < maxval) maxval = v0;
	}
	// apply normalization
	pixGammaTRC(pix, pix, gamma, minval, maxval);
}

l_uint32 dxbot::pixelAdd(l_uint32 a, l_uint32 b) {
	int r_a = a >> 24 & 0xFF,
		g_a = a >> 16 & 0xFF,
		b_a = a >> 8 & 0xFF;
	int r_b = b >> 24 & 0xFF,
		g_b = b >> 16 & 0xFF,
		b_b = b >> 8 & 0xFF;
	return
		(unsigned)(min(255, r_a + r_b)) << 24 |
		(unsigned)(min(255, g_a + g_b)) << 16 |
		(unsigned)(min(255, g_a + g_b)) << 8;
}

l_uint32 dxbot::pixelSub(l_uint32 a, l_uint32 b) {
	int r_a = a >> 24 & 0xFF,
		g_a = a >> 16 & 0xFF,
		b_a = a >> 8  & 0xFF;
	int r_b = b >> 24 & 0xFF,
		g_b = b >> 16 & 0xFF,
		b_b = b >> 8  & 0xFF;
	return
		(unsigned)(std::abs(r_a - r_b)) << 24 |
		(unsigned)(std::abs(g_a - g_b)) << 16 |
		(unsigned)(std::abs(g_a - g_b)) << 8;
}

char * dxbot::Trim(char *s)
{
	if (s) {
		int n = strlen(s);
		int i = n - 1;
		while (i > 0 && isspace(s[i])) i--;
		char *t = new char[i+2];
		memcpy(t, s, i+1);
		t[i+1] = '\0';
		return t;
	}
	return "";
}

const char * dxbot::ScanText(PIX *pix)
{
	PIX *pix1 = pixConvertRGBToGrayFast(pix);
	PIX *pix2 = pixScaleGray2xLI(pix1);
	pixInvert(pix2, pix2);
	pixGammaNorm(pix2, config.text_gamma);
	PIX *pix3 = pixAddBorder(pix2, 16, 255);
	PIX *pix4 = pixUnsharpMaskingGray(pix3, config.text_mask, config.text_fract);

	int w = pixGetWidth(pix4),
		h = pixGetHeight(pix4);
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			l_uint32 val;
			pixGetPixel(pix4, x, y, &val);
			if (val > config.text_bg)
				pixSetPixel(pix4, x, y, 255);
		}
	}

	{
		char file[256];
		static int id = 0;
		sprintf(file, "out/%d.png", id);
		pixWritePng(file, pix4, 1);
		id++;
	}
	
	ocr->SetImage(pix4);
	char *text = ocr->GetUTF8Text();

	pixDestroy(&pix1);
	pixDestroy(&pix2);
	pixDestroy(&pix3);
	pixDestroy(&pix4);

	return Trim(text);
}

PIX* dxbot::AlignToIcon(PIX *img, PIX *icon) {
	int w = pixGetWidth(img),
		h = pixGetHeight(img);
	ColorDistribution st_y0(icon, 0, 0, w, 1);
	ColorDistribution st_yn(icon, 0, h-1, w, 1);
	int y0 = 0;
	int yn = h-1;
	float min_dist_y0 = FLT_MAX;
	float min_dist_yn = FLT_MAX;
	for (int y = 0; y < 2; y++) {
		ColorDistribution st0(img, 0, y, w, 1);
		float d = st_y0.Distance(st0);
		if (d < min_dist_y0) {
			min_dist_y0 = d;
			y0 = y;
		}
	}
	for (int y = h-2; y < h; y++) {
		ColorDistribution stn(img, 0, y, w, 1);
		float d = st_yn.Distance(stn);
		if (d < min_dist_yn) {
			min_dist_yn = d;
			yn = y;
		}
	}
	BOX *box = boxCreate(0, y0, w, yn - y0 + 1);
	PIX *p0 = pixClipRectangle(img, box, NULL);
	PIX *p1 = pixScaleToSize(p0, w, h);
	pixDestroy(&p0);
	return p1;
}

PIX* dxbot::ColorCorrection(PIX *img, PIX *icon) {
	int w = pixGetWidth(img),
		h = pixGetHeight(img);
	PIX *p0 = pixCreate(w, h, 32);
	int sz = 3; // local window size
	int ln = sz / 2; // border
	for (int y = ln; y < h-ln; y++) {
		for (int x = ln; x < w-ln; x++) {
			l_uint32 val;
			pixGetPixel(img, x, y, &val);
			float3 a = float3::pixel(val);
			ColorDistribution g(icon, x - ln, y - ln, sz, sz);
			float3 s{};
			for (int i = -ln; i <= ln; i++) {
				for (int j = -ln; j <= ln; j++) {
					pixGetPixel(icon, x + j, y + i, &val);
					float3 b = float3::pixel(val);
					s += b;
				}
			}
			s = (a - s / (sz*sz)).abs();
			float3 d = (a - g.mean).abs();
			pixGetPixel(icon, x, y, &val);
			pixSetPixel(p0, x, y, d.pixel());
		}
	}
	return p0;
}

const char * dxbot::ScanIcon(PIX *pixs) {
	Icon *icon = FindIcon(pixs);
	if (!icon)
		return "";
	PIX *p0 = icon->pix;
	PIX *p1 = AlignToIcon(pixs, p0);
	PIX *p2 = ColorCorrection(p1, p0);
	int w = pixGetWidth(p0),
		h = pixGetHeight(p0);
	PIX *p3 = pixCreate(w, h, 32);
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			l_uint32 a, b, c;
			pixGetPixel(p0, x, y, &a);
			pixGetPixel(p1, x, y, &b);
			pixGetPixel(p1, x, y, &c);
			float3 v = float3::pixel(pixelSub(a, b));
			v *= float3::pixel(c);
			pixSetPixel(p3, x, y, v.pixel());
		}
	}
	ColorDistribution s(p3,
		config.icon_x, config.icon_y,
		config.icon_w, config.icon_h);
	ColorDistribution t(p3,
		config.icon_text_x, config.icon_text_y,
		config.icon_text_w, config.icon_text_h);
	LOG("DEBUG: %f %f %f %f\n", s.mean.sum(), s.stdev.sum(), t.mean.sum(), t.stdev.sum());
	PIX *p4 = pixCreate(w, h, 8);
	l_uint32 thresh = (s.mean.sum() + 3 * s.stdev.sum()) / (3 * 255);
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			l_uint32 val;
			pixGetPixel(p3, x, y, &val);
			float3 v = float3::pixel(val) - s.mean;
			val = (v.sum() > 3 * s.stdev.sum()) ? 255 : 0;
			pixSetPixel(p4, x, y, val);
		}
	}

	if (config.debug) {
		char file[256];
		static int id = 0;
		sprintf(file, "out/%d-0.png", id);
		pixWritePng(file, p0, 1);
		sprintf(file, "out/%d-1.png", id);
		pixWritePng(file, p1, 1);
		sprintf(file, "out/%d-2.png", id);
		pixWritePng(file, p2, 1);
		sprintf(file, "out/%d-3.png", id);
		pixWritePng(file, p3, 1);
		sprintf(file, "out/%d-4.png", id);
		pixWritePng(file, p4, 1);
		id++;
	}

	ocr->SetImage(p4);
	ocr->SetRectangle(
		config.icon_text_x, config.icon_text_y,
		config.icon_text_w, config.icon_text_h);
	char *text = Trim(ocr->GetUTF8Text());

	int resultsize = icon->name.length() + strlen(text) + 3;
	char *result = new char[resultsize];
	snprintf(result, resultsize, "%s;%s ", icon->name.c_str(), text);

	delete[] text;
	pixDestroy(&p4);
	pixDestroy(&p3);
	pixDestroy(&p2);
	pixDestroy(&p1);

	return result;
}

const char** dxbot::Recognize(ScanBox boxes[], int nboxes, const char *filename)
{
	if (!ocr)
		ocr = new TessBaseAPI();

	if (ocr->Init(NULL, "eng")) {
		LOG("ERROR: Cannot initialize tesseract.\n");
		return nullptr;
	}

	ocr->SetPageSegMode(PSM_SINGLE_LINE);
	ocr->SetVariable("debug_file", "/dev/null");

	PIX *pix_src = filename ? pixRead(filename) : dxbot::DxCapture();
	if (!pix_src) {
		LOG("ERROR: Cannot read image.\n");
		return nullptr;
	}

	const char **result = new const char*[nboxes + 1];
	for (int i = 0; i < nboxes; i++)
		result[i] = "";
	result[nboxes] = nullptr;

	for (int i = 0; i < nboxes; i++) {
		ScanBox &b = boxes[i];
		BOX *box = boxCreate(b.x, b.y, b.w, b.h);
		PIX *pix = pixClipRectangle(pix_src, box, NULL);

		if (b.ocr_whitelist) ocr->SetVariable("tessedit_char_whitelist", b.ocr_whitelist);

		if (b.type == dxbot::DXBOT_SCANICON) {
			result[i] = ScanIcon(pix);
		} else {
			result[i] = ScanText(pix);
		}

		boxDestroy(&box);
		pixDestroy(&pix);
	}

	ocr->Clear();
	pixDestroy(&pix_src);
	return result;
}

void dxbot::FreeResult(const char **v) {
	for (int i = 0; v[i]; i++)
		if (v[i] != "")
			delete[] v[i];
	delete[] v;
}

void dxbot::EndOCR() {
	if (ocr) {
		delete ocr;
		ocr = nullptr;
	}
}
