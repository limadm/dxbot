local ffi = require 'ffi'

ffi.cdef [[
	typedef struct Pix PIX;

	typedef struct {
		int debug;
		int delay_type,
			delay_key,
			delay_mouse,
			delay_click;
		int text_mask,
			text_bg;
		float text_gamma,
			text_fract;
		int icon_size,
			icon_x,
			icon_y,
			icon_w,
			icon_h,
			icon_text_x,
			icon_text_y,
			icon_text_w,
			icon_text_h;
	} Config;

	typedef enum {
		DXBOT_MOUSE1 = 2,
		DXBOT_MOUSE2 = 8,
		DXBOT_MOUSE3 = 32,
	} MouseButton;

	typedef enum {
		DXBOT_SCANTEXT = 1,
		DXBOT_SCANICON = 2,
	} ScanType;

	typedef struct {
		int x, y, w, h;
		ScanType type;
		const char * ocr_whitelist;
	} ScanBox;

	void Init();
	void SetConfig(Config cfg);
	void Type(const char *text);
	void Click(int x, int y, MouseButton button);
	void Wait(int time_ms);
	void StandBy(float time_min);
	const char** Recognize(ScanBox boxes[], int nboxes, const char *filename);
	const char* GetIconDescription(const char *icon, const char *text);
	void FreeResult(const char **);
]]

local function copy(t)
	if type(t) == 'table' then
		local u = {}
		for i,v in pairs(t) do
			u[i] = copy(v)
		end
		return u
	else
		return t
	end
end

local function parse_ocr_cfg(cfg)
	local tboxes = {}
	for i,prop in ipairs(cfg) do
		if prop.size and prop.size > 1 then
			for item=1,prop.size do
				assert(prop.next, '[next] is required when [size] > 1.')
				for j,box in ipairs(prop) do
					local ibox = copy(box)
					ibox[1] = ibox[1] + (item-1) * prop.next[1] -- shift x coordinate
					ibox[2] = ibox[2] + (item-1) * prop.next[2] -- shift y coordinate
					table.insert(tboxes, ibox)
				end
			end
		else
			for j,box in ipairs(prop) do
				table.insert(tboxes, box)
			end
		end
	end
	return tboxes
end

local function id(x)
	return x
end

local function parse_ocr_result(cfg, result, n)
	local T = {}
	local idx = 0
	for i,prop in ipairs(cfg) do
		local parse = prop.parse or id
		local item = {}
		if prop.size then
			for k=1,prop.size do
				item[k] = {}
				for j=1,#prop do
					item[k][j] = ffi.string(result[idx])
					idx = idx+1
				end
				item[k] = parse(item[k])
			end
		else
			for j=1,#prop do
				item[j] = ffi.string(result[idx])
				idx = idx+1
			end
			item = parse(item)
		end
		if prop.name then
			T[prop.name] = item
		else
			table.insert(T, item)
		end
	end
	return T
end

api = {
	MOUSE1 = ffi.C.DXBOT_MOUSE1,
	MOUSE2 = ffi.C.DXBOT_MOUSE2,
	MOUSE3 = ffi.C.DXBOT_MOUSE3,
	TEXT = ffi.C.DXBOT_SCANTEXT,
	ICON = ffi.C.DXBOT_SCANICON,
	init = function (cfg)
		if (cfg) then
			local s = ffi.new('Config', cfg)
			ffi.C.SetConfig(s)
		end
		ffi.C.Init()
	end,
	type  = ffi.C.Type,
	click = ffi.C.Click,
	wait  = ffi.C.Wait,
	standby = ffi.C.StandBy,
	find_name = function (icon, text)
		return ffi.string(ffi.C.GetIconDescription(icon, text))
	end,
	mkocr = function (cfg)
		local tboxes = parse_ocr_cfg(cfg)
		local nboxes = #tboxes
		local boxes = ffi.new('ScanBox[?]', nboxes, tboxes)
		return function (filename)
			local result = ffi.C.Recognize(boxes, nboxes, filename)
			local props = parse_ocr_result(cfg, result, nboxes)
			ffi.C.FreeResult(result)
			return props
		end
	end,
	csv = {
		read = function (filename, parser)
			local t = {}
			local parse = parser or id
			for line in io.lines(filename) do
				local item = {}
				for x in line:gmatch('[^;]+') do
					table.insert(item, x)
				end
				table.insert(t, parse(item))
			end
			return t
		end,
		write = function (t, filename, parser)
			local parse = parser or id
			for i,v in ipairs(t) do
				local item = parse(v)
				io.write(item[j])
				for j=2,#item do
					io.write(';', item[j])
				end
				io.write('\n')
			end
		end,
	},
}
