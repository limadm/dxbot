# dxbot

[dxbot](https://bitbucket.org/dnmario/dxbot) is an interface to
Win32/Direct3D applications (e.g. games), permitting you to script
UI interactions with OCR'ed screenshots and simulated input events.

dxbot uses the Tesseract and Leptonica libraries for OCR, ZLib to 
decompress assets, and LuaJIT for scripting.

--------

dxbot is Copyright under MIT License (c) 2015 Daniel Lima

Tesseract-OCR, Leptonica, ZLib and LuaJIT are copyrighted by their
respective owners. For complete licensing information, see COPYING.