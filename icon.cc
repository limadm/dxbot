#include <cmath>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>

extern "C" {
#include <leptonica/allheaders.h>
}

#include "api.h"
#include "icon.h"

int min3(int a, int b, int c) {
	return std::min(std::min(a, b), c);
}

namespace dxbot
{
	int InitDB();

	std::vector<Icon*> *icons = nullptr;
	std::unordered_map<std::string, Icon*> *icons_by_name = nullptr;
}

using namespace dxbot;

dxbot::Icon::Icon(const std::string &icon) :
	name(icon),
	descs(),
	pix(nullptr),
	n_ft(0)
{
	for (int i = 0; i < n_ch; i++) {
		avg[i] = nullptr;
		sdv[i] = nullptr;
	}
	char filename[256];
	snprintf(filename, sizeof(filename), "db/icons/%s.png", icon.c_str());
	PIX *p = pixRead(filename);
	if (p) {
		pix = pixScaleToSize(p, config.icon_size, config.icon_size);
		pixDestroy(&p);
		createFeatures();
	}
	else {
		fprintf(stderr, "ERROR: cannot read icon %s\n", filename);
	}
}

void dxbot::Icon::createFeatures() {
	n_ft = config.icon_w;
	for (int i = 0; i < n_ch; i++) {
		avg[i] = new float[n_ft];
		sdv[i] = new float[n_ft];
		for (int x = 0; x < n_ft; x++) {
			avg[i][x] = 0;
			sdv[i][x] = 0;
		}
	}
	for (int y = 0; y < config.icon_h; y++) {
		for (int x = 0; x < config.icon_w; x++) {
			l_uint32 val;
			pixGetPixel(pix, config.icon_x + x, config.icon_y + y, &val);
			float r = (val >> 24) / 255.0f;
			float g = (val >> 16) / 255.0f;
			float b = (val >> 8) / 255.0f;
			avg[0][x] += r;
			avg[1][x] += g;
			avg[2][x] += b;
		}
	}
	for (int i = 0; i < n_ch; i++)
		for (int x = 0; x < n_ft; x++)
			avg[i][x] /= config.icon_h;
	for (int y = 0; y < config.icon_h; y++) {
		for (int x = 0; x < config.icon_w; x++) {
			l_uint32 val;
			pixGetPixel(pix, config.icon_x + x, config.icon_y + y, &val);
			float r = (val >> 24) / 255.0f;
			float g = (val >> 16) / 255.0f;
			float b = (val >> 8) / 255.0f;
			sdv[0][x] += (r - avg[0][x]) * (r - avg[0][x]);
			sdv[1][x] += (g - avg[1][x]) * (g - avg[1][x]);
			sdv[2][x] += (b - avg[2][x]) * (b - avg[2][x]);
		}
	}
	for (int i = 0; i < n_ch; i++)
		for (int x = 0; x < n_ft; x++)
			sdv[i][x] = sqrt(sdv[i][x] / config.icon_h);
}

int dxbot::InitDB()
{
	if (!icons) icons = new std::vector<Icon*>(8192);
	if (!icons_by_name) icons_by_name = new std::unordered_map<std::string, Icon*>();
	if (icons->size() > 0) {
		for (Icon *i : *icons)
			delete i;
		icons->clear();
		icons_by_name->clear();
	}
	std::string line;
	std::ifstream file("db/icons.csv");
	while (file && std::getline(file, line)) {
		int split = line.find(';');
		std::string name = line.substr(0, split);
		std::string text = line.substr(split + 1);
		Icon *icon = nullptr;
		if (icons_by_name->count(name)) {
			icon = (*icons_by_name)[name];
		} else {
			icon = new Icon(name);
			icons->push_back(icon);
			(*icons_by_name)[name] = icon;
		}
		icon->descs.push_back(text);
	}
	LOG("INFO: Loaded %d icons.\n", icons->size())
	return 1;
}

int dxbot::TextDistance(const char *a, const char *b)
{
	int a_len = strlen(a),
		b_len = strlen(b);
	int *d[2] = {
		new int[b_len + 1],
		new int[b_len + 1]
	};
	for (int j = 0; j <= b_len; j++)
		d[0][j] = j;
	for (int i = 1; i <= a_len; i++) {
		d[i % 2][0] = i;
		for (int j = 1; j <= b_len; j++) {
			d[i % 2][j] = min3(
				d[(i - 0) % 2][j - 1] + 1,
				d[(i - 1) % 2][j - 0] + 1,
				d[(i - 1) % 2][j - 1] + (a[i - 1] != b[j - 1] ? 1 : 0)
			);
		}
	}
	int dist = d[a_len%2][b_len];
	delete d[0];
	delete d[1];
	return dist;
}

template<typename T, typename num>
T Find(std::vector<T> *list, T key, 
	num (*diff)(T, T, int),
	int maxresults = 10,
	int steps = 1)
{
	std::vector<std::pair<T, num>> m_it, m_next;
	for (auto &p : *list) {
		T &val = p;
		m_it.push_back({ val, diff(key, val, 0) });
	}
	std::sort(m_it.begin(), m_it.end(),
		[](auto &a, auto &b) -> bool {
			return a.second < b.second;
		});
	int k = (int)std::pow(list->size() / maxresults, 1.0 / steps);
	for (int i = 1; i < steps; i++) {
		m_next.clear();
		for (int j = 0; j < m_it.size() / k; j++) {
			T &val = m_it[j].first;
			m_next.push_back({ val, diff(key, val, i) });
		}
		std::sort(m_it.begin(), m_it.end(),
			[](auto &a, auto &b) -> bool {
				return a.second < b.second;
			});
		m_it = m_next;
	}
	T null;
	if (m_it.size() == 0)
		return null;
	return m_it[0].first;
}

Icon* dxbot::FindIcon(PIX *pix) {
	Icon *key = new Icon(pix);
	Icon *found = Find<Icon*, float>(icons, key, [](Icon* a, Icon* b, int i) -> float {
		return a->distance(b, i);
	});
	delete key;
	return found;
}

const char * dxbot::FindIconDesc(Icon *icon, const char *text)
{
	auto &v = icon->descs;
	if (v.size() == 0) return nullptr;
	int min_dist = INT_MAX;
	int min_i = 0;
	for (int i = 0; i < v.size(); i++) {
		int dist = TextDistance(text, v[i].c_str());
		if (dist < min_dist)
			min_i = i;
	}
	return v[min_i].c_str();
}

const char * dxbot::GetIconDescription(const char *icon_name, const char *text)
{
	if (icons_by_name->count(icon_name) == 0)
		return nullptr;
	Icon *icon = (*icons_by_name)[icon_name];
	return FindIconDesc(icon, text);
}