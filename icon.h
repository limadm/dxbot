#pragma once

#include <string>
#include <vector>

extern "C" {
#include <leptonica/allheaders.h>
}

#include "api.h"

namespace dxbot
{
	struct Icon {
		std::string name;
		std::vector<std::string> descs;
		PIX *pix;
		static const int n_ch = 3;
		int n_ft;
		float *avg[n_ch];
		float *sdv[n_ch];

		Icon(const std::string &icon);

		Icon(PIX* pixs) :
			name(),
			pix(pixs),
			n_ft(0)
		{
			if (pixs) {
				createFeatures();
			}
		}

		~Icon() {
			if (n_ft > 0)
				for (int i = 0; i < n_ch; i++) {
					delete[] avg[i];
					delete[] sdv[i];
				}
		}

		void createFeatures();

		float distance(const Icon *that, int level) const {
			if (!this->avg || !that->avg)
				return FLT_MAX;
			float diff = 0;
			for (int i = 0; i < n_ch; i++) {
				for (int j = 0; j < n_ft; j++) {
					diff += (this->avg[i][j] - that->avg[i][j]) * (this->avg[i][j] - that->avg[i][j])
						  + (this->sdv[i][j] - that->sdv[i][j]) * (this->sdv[i][j] - that->sdv[i][j]);
				}
			}
			return sqrt(diff);
		}
	};
}