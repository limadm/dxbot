#pragma once

#define EXPORT __declspec(dllexport)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Pix PIX;

#ifdef __cplusplus
namespace dxbot
{
#endif

	typedef struct {
		int debug;
		int delay_type,
			delay_key,
			delay_mouse,
			delay_click;
		int text_mask,
			text_bg;
		float text_gamma,
			text_fract;
		int icon_size,
			icon_x,
			icon_y,
			icon_w,
			icon_h,
			icon_text_x,
			icon_text_y,
			icon_text_w,
			icon_text_h;
	} Config;

	extern Config config;

	typedef enum {
		DXBOT_MOUSE1 = 2,
		DXBOT_MOUSE2 = 8,
		DXBOT_MOUSE3 = 32,
	} MouseButton;

	typedef enum {
		DXBOT_SCANTEXT = 1,
		DXBOT_SCANICON = 2,
	} ScanType;

	typedef struct {
		int x, y, w, h;
		ScanType type;
		const char * ocr_whitelist;
	} ScanBox;

	EXPORT void Init();
	EXPORT void SetConfig(Config cfg);
	EXPORT void Type(const char *text);
	EXPORT void Click(int x, int y, MouseButton button);
	EXPORT void Wait(int time_ms);
	EXPORT void StandBy(float time_min);
	EXPORT const char** Recognize(ScanBox boxes[], int nboxes, const char *filename);
	EXPORT const char* GetIconDescription(const char *icon_name, const char * text);
	EXPORT void FreeResult(const char **);

#ifdef __cplusplus
	struct Icon;
	int InitDB();
	void DxInit();
	void DxRelease();
	PIX* DxCapture();
	Icon* FindIcon(PIX *pix);
	const char* FindIconDesc(Icon *icon, const char * text);
	void EndOCR();
	int TextDistance(const char *a, const char *b);

#define LOG(...) if (config.debug) fprintf(stderr, __VA_ARGS__);

}
}
#endif