#pragma once

#include <cmath>

namespace dxbot
{

struct float3 {
	float v[3];

	float &operator[] (int i) {
		return v[i];
	}

	float operator[] (int i) const {
		return v[i];
	}

	float3 operator+ (const float3 x) const {
		return{ v[0] + x[0], v[1] + x[1], v[2] + x[2] };
	}
	
	float3 operator+ (float x) const {
		return{ v[0] + x, v[1] + x, v[2] + x };
	}

	float3 operator- (const float3 x) const {
		return{ v[0] - x[0], v[1] - x[1], v[2] - x[2] };
	}
	
	float3 operator- (float x) const {
		return{ v[0] - x, v[1] - x, v[2] };
	}

	float3 operator* (const float3 x) const {
		return{ v[0] * x[0], v[1] * x[1], v[2] * x[2] };
	}

	float3 operator* (float x) const {
		return{ v[0] * x, v[1] * x, v[2] * x };
	}

	float3 operator/ (const float3 x) const {
		return{ v[0] / x[0], v[1] / x[1], v[2] / x[2] };
	}

	float3 operator/ (float x) const {
		return{ v[0] / x, v[1] / x, v[2] / x };
	}

	float sum() const {
		return v[0] + v[1] + v[2];
	}

	float3 &operator+= (const float3 x) {
		v[0] += x[0];
		v[1] += x[1];
		v[2] += x[2];
		return *this;
	}

	float3 &operator-= (const float3 x) {
		v[0] -= x[0];
		v[1] -= x[1];
		v[2] -= x[2];
		return *this;
	}

	float3 &operator*= (float3 x) {
		v[0] *= x[0];
		v[1] *= x[1];
		v[2] *= x[2];
		return *this;
	}

	float3 &operator/= (float x) {
		v[0] /= x;
		v[1] /= x;
		v[2] /= x;
		return *this;
	}

	float3 sqrt() const {
		return{
			std::sqrt(v[0]),
			std::sqrt(v[1]),
			std::sqrt(v[2])
		};
	}

	float3 abs() const {
		return{
			std::abs(v[0]),
			std::abs(v[1]),
			std::abs(v[2])
		};
	}

	static float3 pixel(unsigned int px) {
		return{
			((px >> 24) & 0xFF) / 255.0f,
			((px >> 16) & 0xFF) / 255.0f,
			((px >> 8) & 0xFF) / 255.0f,
		};
	}

	unsigned int pixel() {
		return ((unsigned)(v[0] * 255) << 24)
			| ((unsigned)(v[1] * 255) << 16)
			| ((unsigned)(v[2] * 255) << 8);
	}
};

}
