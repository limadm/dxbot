#include <stdlib.h>
#include <windows.h>

#include "api.h"

using namespace dxbot;

void dxbot::Wait(int time_ms)
{
	constexpr int n = 8;
	int t = 0;
	for (int i = 0; i < n; i++) {
		t += rand() % time_ms;
	}
	Sleep(1 + t / n);
}

void dxbot::Type(const char *text)
{
	Wait(config.delay_type);
	for (int i = 0; text[i] != '\0'; i++) {
		BYTE vk = text[i];
		BYTE sc = MapVirtualKey(vk, MAPVK_VK_TO_VSC);
		Wait(config.delay_key);
		keybd_event(vk, sc, 0, NULL);
		Wait(config.delay_key);
		keybd_event(vk, sc, KEYEVENTF_KEYUP, NULL);
	}
}

void dxbot::Click(int x, int y, MouseButton button) {
	Wait(config.delay_mouse);
	mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE, x, y, 0, NULL);
	if (button) {
		Wait(config.delay_click);
		mouse_event(MOUSEEVENTF_ABSOLUTE | button, x, y, 0, NULL);
		Wait(config.delay_click);
		mouse_event(MOUSEEVENTF_ABSOLUTE | (button << 1), x, y, 0, NULL);
	}
}