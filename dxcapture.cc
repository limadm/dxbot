#include <d3d9.h>
#include <leptonica/allheaders.h>

#include "api.h"

namespace dxbot
{
	static LPDIRECT3D9           d3d = nullptr;
	static LPDIRECT3DDEVICE9     d3d_device = nullptr;
	static LPDIRECT3DSURFACE9    d3d_surface = nullptr;
	static D3DDISPLAYMODE        d3d_mode;
	static D3DPRESENT_PARAMETERS d3d_params;
	static D3DLOCKED_RECT        d3d_rect;
	static LPBYTE	             d3d_buffer = nullptr;
}

using namespace dxbot;

#define CHECK(x) if (FAILED(x)) {DxRelease(); goto end;}

void dxbot::DxInit()
{
	if (!d3d) d3d = Direct3DCreate9(D3D_SDK_VERSION);
	CHECK(d3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3d_mode));
	
	bool invalid = (!d3d_device || !d3d_surface
		|| d3d_mode.Width != d3d_params.BackBufferWidth
		|| d3d_mode.Height != d3d_params.BackBufferHeight);

	if (invalid) {
		DxRelease();

		d3d_params.Windowed = TRUE;
		d3d_params.BackBufferCount = 1;
		d3d_params.BackBufferHeight = d3d_mode.Height;
		d3d_params.BackBufferWidth = d3d_mode.Width;
		d3d_params.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3d_params.hDeviceWindow = NULL;

		CHECK(d3d->CreateDevice(
			D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, NULL, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3d_params, &d3d_device));
		CHECK(d3d_device->CreateOffscreenPlainSurface(
			d3d_mode.Width, d3d_mode.Height, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &d3d_surface, nullptr));

		CHECK(d3d_surface->LockRect(&d3d_rect, NULL, 0));
		UINT pitch = d3d_rect.Pitch;
		CHECK(d3d_surface->UnlockRect());
		d3d_buffer = new BYTE[pitch * d3d_mode.Height];
	}
end:
	return;
}

void dxbot::DxRelease()
{
	if (d3d_buffer) { delete[] d3d_buffer; d3d_buffer = nullptr; }
#define RELEASE(x) if(x) { x->Release(); x=nullptr; }
	RELEASE(d3d_surface);
	RELEASE(d3d_device);
}

PIX* dxbot::DxCapture()
{
	PIX* pix = nullptr;
	DxInit();
	
	if (!d3d_buffer)
		return nullptr;
	
	CHECK(d3d_device->GetFrontBufferData(0, d3d_surface));
	CHECK(d3d_surface->LockRect(&d3d_rect, NULL, 0));
	CopyMemory(d3d_buffer, d3d_rect.pBits, d3d_rect.Pitch * d3d_mode.Height);
	CHECK(d3d_surface->UnlockRect());
	int w = (int)d3d_mode.Width,
		h = (int)d3d_mode.Height;
	pix = pixCreate(w, h, 32);
	
	if (!pix)
		return nullptr;

	LPBYTE p = d3d_buffer;
	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			l_uint32 val = p[0] << 8 | p[1] << 16 | p[2] << 24;
			p += 4;
			pixSetPixel(pix, x, y, val);
		}
	}

end:
	return pix;
}